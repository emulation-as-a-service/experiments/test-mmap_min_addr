#include <stdio.h>
#include <sys/mman.h>

int main() {
  printf("echo 0 | sudo tee /proc/sys/vm/mmap_min_addr\n");
  printf("sudo setsebool -P mmap_low_allowed 1\n");
  mmap((void*)65536, 4096, PROT_READ | PROT_WRITE,
       MAP_SHARED | MAP_ANONYMOUS | MAP_FIXED, -1, 0);
  perror("mmap@65536");
  mmap(0, 4096, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS | MAP_FIXED,
       -1, 0);
  perror("mmap@0");
}
